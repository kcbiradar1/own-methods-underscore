function invert(testObjects) {
    if(typeof testObjects === 'object') {
        let invert_object = {};
        for(let key in testObjects) {
            invert_object[testObjects[key]] = key;
        }
        return invert_object;
    } else {
        return [];
    }
}

module.exports = invert;