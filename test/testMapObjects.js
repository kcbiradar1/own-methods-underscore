const testObjects = require("./object");

const mapObject = require("../mapObject");

function callback(changeProperty, result_object, testObjects) {
  if (changeProperty === "age") {
    result_object[changeProperty] = testObjects[changeProperty] + 20;
  } else {
    result_object[changeProperty] = "Mine";
  }
  return result_object;
}

const result_object = mapObject(testObjects, callback);

console.log(result_object);
