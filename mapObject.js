function mapObject(testObjects, callback) {
  if (typeof testObjects === "object") {
    let result_object = {};
    for (let key in testObjects) {
      result_object = callback(key, result_object, testObjects);
    }
    return result_object;
  } else {
    return {};
  }
}

module.exports = mapObject;
