function values(testObjects) {
  if(typeof testObjects === 'object') {
    let object_values = [];
    for (let key in testObjects) {
      object_values.push(testObjects[key]);
    }
    return object_values;
  } else {
    return [];
  }
}

module.exports = values;
