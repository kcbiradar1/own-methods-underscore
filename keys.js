function keys(testObjects) {
  if(typeof testObjects === 'object') {
    let object_keys = [];
    for (let key in testObjects) {
      object_keys.push(key);
    }

    return object_keys;
  } else {
    return [];
  }
}

module.exports = keys;
