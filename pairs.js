function pairs(testObjects) {
  if(typeof testObjects === 'object') {
    let paris_result = [];
    for (let key in testObjects) {
      paris_result.push([key, testObjects[key]]);
    }
    return paris_result;
  } else {
    return [];
  }
}

module.exports = pairs;
