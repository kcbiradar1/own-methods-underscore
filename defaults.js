function defaults(testObjects , defaultProps) {
  if(typeof testObjects === 'object') {
    let new_test_object = {}
    for(var key in testObjects) {
      new_test_object[key] = testObjects[key];
    }
    for(var key in defaultProps) {
      if(!testObjects[key]) {
        new_test_object[key] = defaultProps[key];
      }
    }

    return new_test_object;
  } else {
    console.log(`Provide the correct argument for function`);
  }
}

module.exports = defaults;
